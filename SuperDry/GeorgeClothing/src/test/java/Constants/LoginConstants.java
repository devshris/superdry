package Constants;

import org.openqa.selenium.By;

public interface LoginConstants {
	
	static By LOGINICON = By.cssSelector(".custom-icon.my-account");
	static By USERNAME = By.cssSelector("#username_login");
	static By PASSWORD = By.cssSelector("#passwd_login");
	static By LOGINBUTTON = By.cssSelector("input[name='Submit']");
	

}
