package Constants;

import org.openqa.selenium.By;

public interface SearchConstants {

	 static By SEARCHTEXTBOX1 = By.cssSelector(".search-input.js-dummy-input");
	 static By SEARCHTEXTBOX2 = By.cssSelector("#pop-in-search-term");
	 
	 static By SEARCHBUTTON = By.cssSelector(".custom-icon.block-icon.search-icon");
}
