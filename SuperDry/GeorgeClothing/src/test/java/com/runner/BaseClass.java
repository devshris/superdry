package com.runner;

import org.openqa.selenium.WebDriver;

import com.pages.EndToEndFunction;
import com.pages.HomePage;
import com.pages.LoginPage;
import com.pages.MyAccountPage;
import com.pages.SearchResultPage;

import Constants.LoginConstants;
import Constants.SearchConstants;

public class BaseClass implements SearchConstants,LoginConstants{

	public static WebDriver driver;
	
	public static String globalSearchWord;
	
	public static HomePage homePage = new HomePage();
	
	public static SearchResultPage searchResultPage = new SearchResultPage();
	
	public static LoginPage loginPage = new LoginPage();

	public static MyAccountPage myAccountPage = new MyAccountPage();
	
	public static EndToEndFunction endToEnd=new EndToEndFunction();
	
}