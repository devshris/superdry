package com.stepDefinition;

import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends BaseClass {
	
	String URL="https://www.superdry.com/";
	
	@Before
	public void start() {
	  System.setProperty("webdriver.chrome.driver","C:\\Users\\CEX\\Downloads\\chromedriver_win32 (3)\\chromedriver.exe");
      driver = new ChromeDriver();
      driver.get(URL);
	}

	//@After
	//public void close() {
		//driver.close();
		
	}

