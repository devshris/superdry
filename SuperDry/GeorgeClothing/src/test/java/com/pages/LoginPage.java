package com.pages;

import java.util.Map;

import org.junit.Assert;

import com.runner.BaseClass;

import cucumber.api.DataTable;

public class LoginPage extends BaseClass{
	
	public void verifyLoginPage() {
		
		driver.findElement(LOGINICON).click();
		Assert.assertEquals("https://www.superdry.com/my-account",driver.getCurrentUrl());
	}

	public void verifyLoginWithValidDetails(DataTable loginTable) {
		
		Map<String, String> loginDetails=loginTable.asMap(String.class, String.class);
		
		
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys(loginDetails.get("email"));
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys(loginDetails.get("password"));
		driver.findElement(LOGINBUTTON).click();
		}
	
public void verifyLoginWithInvalidDetails(DataTable loginTable) throws InterruptedException {
		
		Map<String, String> loginDetails=loginTable.asMap(String.class, String.class);
		
		Thread.sleep(3000);
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys(loginDetails.get("email"));
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys(loginDetails.get("password"));
		driver.findElement(LOGINBUTTON).click();
		}
	
	
}
