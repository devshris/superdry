package com.pages;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import com.runner.BaseClass;

public class HomePage extends BaseClass {
	
	
public void verifyHomePage() {
	Assert.assertEquals("Superdry Summer Sale Now On - Mens & Womens Clothing - Superdry", driver.getTitle());
}

public void searchForTheValidProducts(String search)
{
	globalSearchWord=search;
	driver.findElement(SEARCHTEXTBOX1).click();;
	driver.findElement(SEARCHTEXTBOX2).clear();
	driver.findElement(SEARCHTEXTBOX2).sendKeys(search);
	driver.findElements(SEARCHBUTTON).get(1).click();
}


public void searchForTheInvalidProducts(String search) {

	globalSearchWord=search;
	driver.findElement(SEARCHTEXTBOX1).click();
	driver.findElement(SEARCHTEXTBOX2).clear();
	driver.findElement(SEARCHTEXTBOX2).sendKeys(search);
	driver.findElements(SEARCHBUTTON).get(1).click();
	
}

public void searchWithAutoFill() {
	driver.findElement(SEARCHTEXTBOX1).click();
	driver.findElement(SEARCHTEXTBOX2).clear();
	driver.findElement(SEARCHTEXTBOX2).sendKeys("wom");	
	for (int i=0; i<=3; i++) {
		driver.findElement(SEARCHTEXTBOX2).sendKeys(Keys.ARROW_DOWN);
		}
	driver.findElement(SEARCHTEXTBOX2).sendKeys(Keys.ENTER);
	
}
}





